# monitor
Scripts for monitoring health of a home server, written in J.


- `temperature` takes the temp of several components in the computer: hard drives, GPU, CPU, etc.
   It then shifts those readings onto the end of a log file `templog.txt` which can be examined for anomalies.
- `plot-temp` reads `templog.txt` and makes a PDF of the values, discarding timestamp info.
- `dns` grabs the machine's public IP address using `curl`.

The idea is to run `temperature` every 5 minutes via `cron`, and run `plot-temp` every 24 hours, to visually check the last day's worth of temperatures.

